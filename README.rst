This application crawls stores all the popular wordpress blogs, their up-to-date posts and comments. It also stores those data onto database. It uses concurrency to make the process faster.


Prerequisites:
--------------
	MongoDB needs to be installed. Please check the schema for required structure.


Configuration:
--------------
	Modify various parameters in classes.py file


How to Run:
-----------
	python main.py
